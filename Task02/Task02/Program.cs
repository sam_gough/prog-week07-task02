﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class Program
    {
        static void Main(string[] args)
        {
            greeting();
            greeting();
            greeting();
            greeting();
        }

        static void greeting()
        {
            Console.WriteLine("Hello");
            Console.WriteLine("This second line is now printed on the screen");
        }
    }
}
